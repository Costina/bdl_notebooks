from setuptools import setup

setup(
    name='bdl_notebooks',
    version='0.1',
    platforms=['Jupyter Notebook'],
    packages=[
        'bdl_notebooks'
    ],
    url='https://bitbucket.org/Costina/bdl_notebooks/src/master/',
    include_package_data=True,
    install_requires=[
        'notebook>=4.2.0',
        'nbconvert',
        'nbformat'
    ]
)