import os
import json
import subprocess as sp
import itertools
import nbformat
from bdl_client.rest import ApiException
from tornado import web
from nbconvert import HTMLExporter, RSTExporter
from nbconvert.exporters.script import ScriptExporter
from notebook.utils import url_path_join as ujoin, to_os_path
from notebook.base.handlers import IPythonHandler
from traitlets import Unicode, Bool
from traitlets.config import LoggingConfigurable
from .utils.ipycompat import HasTraits
from base64 import b64encode, b64decode
from .utils.ipycompat import writes
from .api_utils import (
    reads_base64,
    writes_base64,
    notebook_model_from_db,
)
from PIL import Image
try:
    from BytesIO import BytesIO
except ImportError:
    from io import BytesIO
try:
    from StringIo import StringIO
except ImportError:
    from io import StringIO
from bdl_client.api.notebooks_public_api_controller_api import NotebooksPublicApiControllerApi
from bdl_client.models.notebook import Notebook


class Examples(LoggingConfigurable, HasTraits):

    user_id = Unicode(
        allow_none=True,
        config=True,
        help="Name for the user whose contents we're managing",
    )

    extract_images = Bool(
        allow_none=True,
        config=True,
        default_value=True,
        help="Extract figures and attachments from the published notebook",
    )

    def __init__(self, *args, **kwargs):
        super(Examples, self).__init__(*args, **kwargs)
        self.api = NotebooksPublicApiControllerApi()

    def make_unique_filename(self, abs_fn, insert='-Clone'):
        path, filename = os.path.split(abs_fn)
        basename, ext = os.path.splitext(filename)
        for i in itertools.count():
            insert_i = f'{insert}{i}'
            name = f'{basename}{insert_i}{ext}'
            new_abs_fn = os.path.join(path, name)
            if not os.path.exists(new_abs_fn):
                break
        return new_abs_fn

    def list_examples(self):
        all_examples = []
        try:
            result = self.api.get_all_using_get()
        except ApiException as e:
            raise web.HTTPError(e.status, json.loads(e.body)['message'])
        examples = [{'filename': row.name, 'user': row.user_owner_id, 'content': row.content_base64_encoded, 'createdAt': row.created_at} for row in result]
        for example in examples:
            basename, ext = os.path.splitext(example['filename'])
            example['basename'] = basename
            example['owned'] = example['user'] == int(self.user_id)
            date = example['createdAt']
            example['createdAt'] = f"{date.year}-{date.month}-{date.day}"
        all_examples.extend(examples)
        return all_examples


    def fetch_example(self, user_id, dest, notebook_name, contents_manager):
        abs_dest = to_os_path(dest)
        if not abs_dest.endswith('.ipynb'):
            abs_dest = f'{abs_dest}.ipynb'
        basename = os.path.basename(abs_dest)
        if os.path.exists(abs_dest):
            abs_dest = self.make_unique_filename(abs_dest)
        try:
            result = self.api.get_using_get(user_id, notebook_name, subscriber=self.user_id)
        except ApiException as e:
            raise web.HTTPError(e.status, json.loads(e.body)['message'])
        model = notebook_model_from_db(result)
        contents_manager.save(model, abs_dest)
        if self.extract_images:
            node = nbformat.read(abs_dest, nbformat.NO_CONVERT)
            self._extract_attachements(node)
            p = sp.Popen(['jupyter', 'nbconvert', abs_dest,
                        '--execute', '--inplace',
                        '--ExecutePreprocessor.allow_errors='+str(True)],
                        stdout=sp.PIPE, stderr=sp.PIPE)
            output, err = p.communicate()
            retcode = p.poll()
            if retcode != 0:
                raise RuntimeError('jupyter nbconvert exited with error {}'.format(err))
            else:
                node = nbformat.read(abs_dest, nbformat.NO_CONVERT)
                self._extract_figures(node)
                p = sp.Popen(['jupyter', 'nbconvert', abs_dest,
                    '--ClearOutputPreprocessor.enabled='+str(True), '--inplace'])
                output, err = p.communicate()
                sp.Popen(['jupyter', 'trust', abs_dest])
        return abs_dest

    def submit_example(self, user_filepath, contents_manager):
        directory, name = os.path.split(user_filepath)
        node = nbformat.read(user_filepath, nbformat.NO_CONVERT)
        content = writes_base64(node).decode('utf8')
        notebook = Notebook(content, None, 'ipynb', name, None, self.user_id)
        try:
            result = self.api.create_using_post(self.user_id, notebook)
        except ApiException as e:
            raise web.HTTPError(e.status, json.loads(e.body)['message'])
        _script_exporter = ScriptExporter(parent=contents_manager)
        os_path = to_os_path(user_filepath, contents_manager.root_dir)
        basename = os.path.basename(os_path)
        basename, ext = os.path.splitext(basename)
        script, resources = _script_exporter.from_filename(os_path)
        script_fname = f'{basename}{resources.get("output_extension", ".txt")}'
        content = b64encode(script.encode()).decode('utf8')
        notebook = Notebook(content, None, 'py', script_fname, None, self.user_id)
        try:
            result = self.api.create_using_post(self.user_id, notebook)
        except ApiException as e:
            raise web.HTTPError(e.status, json.loads(e.body)['message'])
        p = sp.Popen(['jupyter', 'nbconvert', user_filepath,
                    '--ClearOutputPreprocessor.enabled='+str(True), '--inplace'])
        return name

    def exists(self, user_filepath):
        directory, name = os.path.split(user_filepath)
        try:
            result = self.api.get_using_head(self.user_id, name, _preload_content=False)
        except ApiException:
            return False
        return True

    def preview_example(self, notebook_name, user_id, contents_manager):
        try:
            result = self.api.get_using_get(user_id, notebook_name)
        except ApiException as e:
            raise web.HTTPError(e.status, json.loads(e.body)['message'])
        html_exporter = HTMLExporter()
        (body, resources) = html_exporter.from_notebook_node(reads_base64(result.content_base64_encoded))
        return body

    def delete_example(self, filename):
        try:
            result = self.api.delete_using_delete(self.user_id, filename)
        except ApiException as e:
            raise web.HTTPError(e.status, json.loads(e.body)['message'])
        filename = ".py".join(filename.rsplit(".ipynb", 1))
        try:
            result = self.api.delete_using_delete(self.user_id, filename)
        except ApiException as e:
            raise web.HTTPError(e.status, json.loads(e.body)['message'])

    def update_example(self, user_filepath, contents_manager):
        directory, name = os.path.split(user_filepath)
        node = nbformat.read(user_filepath, nbformat.NO_CONVERT)
        content = writes_base64(node).decode('utf8')
        notebook = Notebook(content, None, 'ipynb', name, None, self.user_id)
        try:
            result = self.api.update_using_put(self.user_id, notebook)
        except ApiException as e:
            raise web.HTTPError(e.status, json.loads(e.body)['message'])
        _script_exporter = ScriptExporter(parent=contents_manager)
        os_path = to_os_path(user_filepath, contents_manager.root_dir)
        basename = os.path.basename(os_path)
        basename, ext = os.path.splitext(basename)
        script, resources = _script_exporter.from_filename(os_path)
        script_fname = f'{basename}{resources.get("output_extension", ".txt")}'
        content = b64encode(script.encode()).decode('utf8')
        notebook = Notebook(content, None, 'py', script_fname, None, self.user_id)
        try:
            result = self.api.update_using_put(self.user_id, notebook)
        except ApiException as e:
            raise web.HTTPError(e.status, json.loads(e.body)['message'])
        sp.Popen(['jupyter', 'trust', user_filepath])
        return name

    def _extract_attachements(self, node):
        images = []
        for cell in node['cells']:
            if 'attachments' in cell:
                attachments = cell['attachments']
                for filename, attachment in attachments.items():
                    for mime, base64 in attachment.items():
                        images.append( {'attachment':f'{filename}', 'data':f'{mime}', 'base64':f'{base64}'})
        for image in images:
            basename, ext = os.path.splitext(image['attachment'])
            ext = ext.replace(".", "")
            with Image.open(BytesIO(b64decode(image['base64']))) as img:
                img.save(image['attachment'], ext)

    def _extract_figures(self, node):
        rst_exporter = RSTExporter()
        (body, resources) = rst_exporter.from_notebook_node(node)
        for i in resources['outputs'].keys():
            basename, ext = os.path.splitext(i)
            ext = ext.replace(".", "")
            with Image.open(BytesIO(resources['outputs'][i])) as img:
                img.save(i, ext)

class BaseExampleHandler(IPythonHandler):

    @property
    def manager(self):
        return self.settings['example_manager']


class ExamplesHandler(BaseExampleHandler):

    @web.authenticated
    def get(self):
        self.finish(json.dumps(self.manager.list_examples()))


class ExampleActionHandler(BaseExampleHandler):
    @web.authenticated
    def get(self, action):
        filename = self.get_argument('filename')
        if action == 'preview':
            user_id = self.get_argument('user_id')
            self.finish(self.manager.preview_example(filename, user_id, self.contents_manager))
        elif action == 'fetch':
            dest = self.get_argument('dest')
            user_id = self.get_argument('user_id')
            dest = self.manager.fetch_example(user_id, dest, filename, self.contents_manager)
            self.redirect(ujoin(self.base_url, 'notebooks', dest))
        elif action == 'submit':
            self.finish(json.dumps(self.manager.submit_example(filename, self.contents_manager)))
        elif action == 'delete':
            self.manager.delete_example(filename)
            self.redirect(ujoin(self.base_url, 'tree#examples'))
        elif action == 'exist':
            self.finish(json.dumps(self.manager.exists(filename)))
        elif action == 'update':            
            self.finish(json.dumps(self.manager.update_example(filename, self.contents_manager)))


# -----------------------------------------------------------------------------
# URL to handler mappings
# -----------------------------------------------------------------------------


_example_action_regex = r"(?P<action>fetch|preview|submit|delete|exist|update)"

default_handlers = [
    (r"/examples", ExamplesHandler),
    (r"/examples/%s" % _example_action_regex, ExampleActionHandler),
]


def load_jupyter_server_extension(nbapp):
    """Load the nbserver"""
    webapp = nbapp.web_app
    webapp.settings['example_manager'] = Examples(parent=nbapp)
    base_url = webapp.settings['base_url']

    ExampleActionHandler.base_url = base_url
    webapp.add_handlers(".*$", [
        (ujoin(base_url, pat), handler)
        for pat, handler in default_handlers
    ])