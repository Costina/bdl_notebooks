from base64 import (
    b64decode,
    b64encode,
)
import mimetypes
from .utils.ipycompat import reads, writes


NBFORMAT_VERSION = 4


def base_model(path):
    return {
        "name": path.rsplit('/', 1)[-1],
        "path": path,
        "writable": True,
        "last_modified": None,
        "created": None,
        "content": None,
        "format": None,
        "mimetype": None,
    }

def notebook_model_from_db(record):
        """
        Build a notebook model from database record.
        """
        path = record.name
        model = base_model(path)
        model['type'] = 'notebook'
        model['last_modified'] = model['created'] = record.created_at
        content = reads_base64(record.content_base64_encoded)
        model['content'] = content
        model['format'] = 'json'
        return model


def to_api_path(db_path):
    """
    Convert database path into API-style path.
    """
    return db_path.strip('/')


def writes_base64(nb, version=NBFORMAT_VERSION):
    """
    Write a notebook as base64.
    """
    return b64encode(writes(nb, version=version).encode('utf-8'))


def reads_base64(nb, as_version=NBFORMAT_VERSION):
    """
    Read a notebook from base64.
    """
    try:
        return reads(b64decode(nb).decode('utf-8'), as_version=as_version)
    except Exception as e:
        raise CorruptedFile(e)