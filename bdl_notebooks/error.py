"""
Errors and exceptions.
"""

class CorruptedFile(Exception):
    pass
