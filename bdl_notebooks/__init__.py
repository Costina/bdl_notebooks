try:
    import IPython  # noqa
except ImportError:
    raise ImportError(
        "No IPython installation found.\n"
        "To install pgcontents with the latest Jupyter Notebook"
        " run 'pip install pgcontents[ipy4]b'.\n"
        "To install with the legacy IPython Notebook"
        " run 'pip install pgcontents[ipy3]'.\n"
    )

def _jupyter_server_extension_paths():
    """Returns server extension metadata to notebook 4.2+"""
    return [{
        'module': 'bdl_notebooks.handlers'
    }]


def _jupyter_nbextension_paths():
    """Returns frontend extension metadata to notebook 4.2+"""
    return [{
        'section': 'notebook',
        'src': 'static',
        'dest': 'bdl_notebooks',
        'require': 'bdl_notebooks/submit-example-button'
    }, {
        'section': 'tree',
        'src': 'static',
        'dest': 'bdl_notebooks',
        'require': 'bdl_notebooks/main'
    }]